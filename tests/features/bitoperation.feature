@libraries=bitoperation
Feature: Bit and bitmask operations

Scenario: Convert small int to binary array

    Given a numeric value of "3"
    when I convert the numeric value to a binary array padded to "64" values
    then the binary array should equal "0000000000000000000000000000000000000000000000000000000000000011"

Scenario: Convert small int to binary array with fewer values

    Given a numeric value of "3"
    when I convert the numeric value to a binary array padded to "53" values
    then the binary array should equal "00000000000000000000000000000000000000000000000000011"

Scenario: Convert very small int to binary array with fewer values

    Given a numeric value of "1"
    when I convert the numeric value to a binary array padded to "53" values
    then the binary array should equal "00000000000000000000000000000000000000000000000000001"

Scenario: Convert medium-sized int to binary array

    Given a numeric value of "500"
    when I convert the numeric value to a binary array padded to "64" values
    then the binary array should equal "0000000000000000000000000000000000000000000000000000000111110100"

Scenario: Convert large int to binary array // fits in 29 bits

    Given a numeric value of "285212672"
    when I convert the numeric value to a binary array padded to "64" values
    then the binary array should equal "0000000000000000000000000000000000010001000000000000000000000000"

Scenario: Convert large unsigned long long to binary array // largest = 9007199254740991 ( 2^53 )

    Given a numeric value of "9007199254740991"
    when I convert the numeric value to a binary array padded to "53" values
    then the binary array should equal "11111111111111111111111111111111111111111111111111111"

Scenario: Convert slightly less large to binary array // largest = 9007199254740991 ( 2^53 )

    Given a numeric value of "9007199254740990"
    when I convert the numeric value to a binary array padded to "53" values
    then the binary array should equal "11111111111111111111111111111111111111111111111111110"

Scenario: Reverse binary array

    Given a binary array of "0000000000000000000000000000000000000000000000000000000000001011"
    when I reverse the array
    then the binary array should equal "1101000000000000000000000000000000000000000000000000000000000000"

Scenario: Get index array from bit mask

    Given a binary array of "1101000000000000000000000000000000000000000000000000000000000000"
    when I get an index array from the bit mask
    then the index array should equal "0,1,3"

