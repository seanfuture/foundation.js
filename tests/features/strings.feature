@libraries=strings
Feature: Strings extension library

Scenario: Test string

    Given a test string value of "mystring"
    when that string is run through a test conversion
    then the resulting string would be "test - mystring"

Scenario: Sample string test conversion

    Given a test string value of "Sample"
    when that string is run through a test conversion
    then the resulting string would be "test - Sample"

Scenario: Pie string test conversion

    Given a test string value of "PIE"
    when that string is run through a test conversion
    and that string is run through a test conversion
    then the resulting string would be "test - test - PIE"

Scenario: Uppercase first letter

    Given a test string value of "sample test"
    when that string is run through an upperCaseFirstLetter conversion
    then the resulting string would be "Sample test"
