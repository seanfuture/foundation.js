var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    should = require('should'),
    dictionary = require('../dictionary'),
    mongo = require('../../lib/mongo');

module.exports = (function() {

    var _mongoConfig;

    var library = English.library(dictionary)

        .given("a valid Mongo DB extension library", function(next) {
            next();
        })

        .when("a connection string of \"$INPUT\" is parsed", function(input, next) {
            _mongoConfig = mongo.parseConnectionString( input );
            next();
        })

        .then("the resulting configuration database name should be \"$INPUT\"", function(input, next) {
            _mongoConfig.databaseName.should.equal( input );
            next();
        })

    return library;
})();
