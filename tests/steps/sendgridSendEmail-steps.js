// var assert = require('assert'),
// 	English = require('yadda').localisation.English,
// 	SendGrid = require('../../lib/providers/sendgrid'),
//     inspect = require('util').inspect;

// module.exports = (function(){
// 	var _to = '',
// 		_from = '',
// 		_subject = '',
// 		_body = '',
// 		_provider;

// 	return English.library()
// 	.given('a valid EmailProvider', function(next){
// 		_provider = new SendGrid('username', 'password');
// 		next();
// 	})
// 	.when('I create a message to $to', function(to, next){
// 		_to = to;
// 		next();
// 	})
// 	.when('from address of $from', function(from, next){
// 		_from = from;
// 		next();
// 	})
// 	.when('subject of \"$subject\"', function(subject, next){
// 		_subject = subject;
// 		next();
// 	})
// 	.when('text body of \"$body\"', function(body, next){
// 		_body = body;
// 		next();
// 	})
// 	.then('I send the message', function(next){
// 		_provider.SendEmail(_to, _from, _subject, _body, false, next);
// 	})
// })();