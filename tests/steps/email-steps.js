var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    dictionary = require('../dictionary'),
    provider = null;

module.exports = (function() {

    var library = English.library(dictionary)

        .given("a valid email extension", function( next ) {
            email = require('../../lib/email')
            next()
        })

        .given("a valid bulk email extension", function( next ) {
            bulkemail = require('../../lib/bulkEmail');
            next()
        })

        .when("a test email is sent with subject \"$INPUT\"", function( input, next ) {
            //email.sendMail( undefined, next );
        })

        .when("a test bulk email is sent with subject \"$INPUT\"", function( input, next ) {
            var e = {
                from: 'welcome@test.com',
                subject: 'Welcome to test™',
                text: 'Welcome to test™ - Content Test 1',
                html: '<h3>Welcome to test™</h3><p>Content Test 1</p>'
            };
            var recipients = [
                "inbox@seanfuture.com"
            ];
            // email.sendMail( e, "ActivateAccount", recipients, next );
        })

        .then("an email should be verified as sent with subject \"$INPUT\"", function( input, next ) {
            // assert.equal(input, email);
            next();
        })

    return library;
})();
