
# Foundation.js

JavaScript extension library designed for use in both the *browser* and *Node.js*

## Features

Foundation.js contains a number of extension libraries useful for development:

* Strings.js
* Datetime.js
* TBD


## Dependencies

* Lodash.js ( <http://lodash.com> )
* TBD


## Installation

TBD