var Yadda = require('yadda'),
    English = Yadda.localisation.English,
    assert = require('assert'),
    dictionary = require('../dictionary'),
    JobQueue = require('../../lib/jobqueue');

module.exports = (function() {

    var queue;

    return English.library(dictionary)

        .given("a new job", function(next) {
            queue = new JobQueue();
            next();
        })

        .when("I add a job to the test queue", function(next) {
            // queue.Add('test', { type: 'test' }, next);
            next();
        })

        .when("I process the test queue", function(next) {
            next();
            // queue.Process('test', function(job, done){
            //     // done(); -- Unit test reporting multiple calls to done()
            //     next();
            // })
        })

        .then("that queue should have a job in it with the status of $status", function(status,next) {
            queue.Fetch({status : status, data : {type : 'test'}},function(jobs){
                that = queue;

                if(jobs.length === 0 && jobs.length > 1) assert.fail(0,1,"No Job Found", null);

                for(var job in jobs._data)
                    that.Cancel(job.substring(job.lastIndexOf('/') + 1));

                next();
            })
        });
})();