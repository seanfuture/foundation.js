
var kinetic = require( 'kinetik' ),
    mongo = require( 'seed-mongodb' ),
    config = require( 'config' ),
    queue;

function JobQueue() {

    if( config.jobqueue == undefined )
        config.jobqueue = {
            database: {
                name: 'jobs',
                host: 'localhost',
                port: 27017
            }
        };

    // RE: https://github.com/lorenwest/node-config/wiki/Common-Usage
    queue = kinetic( new mongo( {
        db: config.jobqueue.database.name,
        host: config.jobqueue.database.host,
        port: config.jobqueue.database.port
    } ) );

}

JobQueue.prototype.Add = function( queueName, dataObj, callback ) {
    queue.create( queueName, dataObj );
    queue.on( 'error', function( err ) {
        if ( err ) throw err;
    } );
    queue.on( 'drain', function() {
        if ( callback ) callback();
    } );
};

JobQueue.prototype.Clear = function( queueName, callback ) {
    queue.clean( queueName, function( err ) {
        if ( err ) throw err;
        if ( callback ) callback();
    } );
};

JobQueue.prototype.Get = function( query, fetchAction ) {
    queue.fetch( query, function( err, jobs ) {
        if ( err ) throw err;
        fetchAction( jobs );
    } );
};

JobQueue.prototype.Process = function( queueName, taskAction ) {
    queue.define( queueName ).tag( queueName.replace( / /g, '' ) ).action( function( job, done ) {
        if ( taskAction ) taskAction( job, done );
    } );
    queue.process( queueName );
};

JobQueue.prototype.Cancel = function( job, callback ) {
    queue.cancel( job, function( err ) {
        if ( err ) throw err;
        if ( callback ) callback();
    } );
};

module.exports = JobQueue;
