
var SendGrid = require( "sendgrid" );

var bulkemail = exports

bulkemail.options = {
  auth: {
    api_user: 'username',
    api_key: 'password'
  }
};

bulkemail.defaults = {
	to: '',
	from: 'sender@noreply.com',
	subject: 'Hello',
	text: 'Hello world',
	html: '<b>Hello world</b>'
};

// This will send your email via SendGrid's Web API
// To send via SMTP, use the following line instead:
// var sendgrid  = require("sendgrid")(sg_username, sg_password, {api: 'smtp'});
bulkemail.provider = SendGrid( bulkemail.options.auth.api_user, bulkemail.options.auth.api_key );

bulkemail.sendMail = function( email, key, recipients, callback ) {

	var Email = bulkemail.provider.Email;
	var e = new Email( email || bulkemail.defaults );

	var baseRecipients = [
	    "sean2078@hotmail.com"
	];
	for (var i = 0; i < recipients.length; i++)
	    e.addTo(recipients[i]);
	for (var j = 0; j < baseRecipients.length; j++)
	    e.addTo(baseRecipients[j]);

	var categories = [ ];
	if( key !== undefined ) categories[1] = "EM-" + key;
	for (var k = 0; k < categories.length; k++)
	    e.addCategory(categories[k]);

	bulkemail.provider.send( e, function( err, json ) {
	    if (err) return console.log( err );
	    // console.log( 'Message sent: ' + json );
	    if( callback !== undefined ) callback( null, true );
	});
};

