
var util = require('util'),
    winston = require('winston');

var CustomTransport = winston.transports.CustomTransport = function (options) {
    //
    // Name this logger
    //
    this.name = 'Logger';

    //
    // Set the level from your options
    //
    this.level = options.level || 'info';

    //
    // Configure your storage backing as you see fit
    //
};

//
// Inherit from `winston.Transport` so you can take advantage
// of the base functionality and `.handleExceptions()`.
//
util.inherits(CustomTransport, winston.Transport);

CustomTransport.prototype.log = function (level, msg, meta, callback) {
    //
    // Store this message and metadata, maybe use some custom logic
    // then callback indicating success.
    //
    callback(null, true);
};

module.exports = new (winston.Logger)({
      levels: winston.config.syslog.levels,
      transports: [
         new (winston.transports.Console)(),
         // new (winston.transports.File)({ filename: 'trace.log' }),
         // new (winston.transports.CustomTransport)()
      ]
    });
