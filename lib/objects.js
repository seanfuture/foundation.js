
var _ = require( 'lodash' ),
    qs = require('querystring')

// global extensions to "object" are defined on the "Object.prototype"
// NOTE - DO NOT extend Object at this time due to MongoDB lib errors
// .. from reflection scans when extending

var objects = exports

objects.isEmpty = function( o ) {
	if( ! o ) return true;
    return Object.keys( o ).length === 0;
}

objects.querystring = function( obj ) {
    return qs.stringify( obj )
}

objects.keys = function( obj ) {
    return Object.keys( obj )
}


