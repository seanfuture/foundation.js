
var math = exports

/**
 * Round a number to a desired number of digits
 *
 * @param  {Number} n Input number
 * @param  {Number} d Number of digits to round to
 * @return {Number}   Result
 */
math.round = function( n, d ) {
    if( ! d ) d = 2;
    return parseFloat( parseFloat(
        Math.round( n * 100 ) / 100 ).toFixed( d ) )
}

math.percentage = function( n ) {
	return math.round( n * 100 )
}


