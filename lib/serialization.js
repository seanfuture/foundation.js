
var _ = require( 'lodash' ),
    qs = require('querystring')

var serialization = exports

serialization.querystring = function( obj ) {
    return qs.stringify( obj )
}

