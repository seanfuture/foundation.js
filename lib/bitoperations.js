
var strings = require( '../lib/strings.js' );

var bitops = exports

bitops.getIndexArrayFromBitmask = function( bitmask ) {
    var r = [];
    var a = bitmask.split('');
    for (var i = 0; i < a.length; i++)
        if( a[i] == '1' ) r.push( i );
    return r;
};

bitops.reverse = function( s ) {
    return s.split('').reverse().join('');
};

bitops.get64binary = function( int ) {
	return this.getbinary( int, 64 );
};

bitops.getbinary = function( int, length ) {

	// RE: http://stackoverflow.com/questions/10936600/javascript-decimal-to-binary-64-bit
    if( int >= 0 )
    	return String(int.toString( 2 )).padLeft( length, "0" );
    	// return int.toString( 2 ).test();

    return ( -int - 1 ).toString( 2 ).replace(/[01]/g, function( d ) {
            return +!+d;
        }) // inverts each char
        .padLeft( length, "1" );
};


