
var nodemailer = require('nodemailer'),
	sgTransport = require('nodemailer-sendgrid-transport');

var email = exports

email.options = {
  auth: {
    api_user: 'username',
    api_key: 'password'
  }
}

email.client = nodemailer.createTransport(sgTransport( email.options ));

email.defaults = {
	to: 'inbox@seanfuture.com',
	from: 'inbox@seanfuture.com',
	subject: 'Hello',
	text: 'Hello world',
	html: '<b>Hello world</b>'
};

email.sendMailSync = function( message ) {
    var e = ( message || email.defaults );
	email.client.sendMail( e, callback );
	// email.client.sendMail( e, function( err, info ) {
	//     if( err )
	//       console.log(error);
	//     else
	//       console.log('Message sent: ' + info.response);
	// });
}

email.sendMail = function( message, callback ) {
    var e = ( message || email.defaults );
	email.client.sendMail( e, callback );
}

