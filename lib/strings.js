
// global extensions to "string" are defined on the "String.prototype"

var strings = exports

String.prototype.reverse = function() {
    return this.split('').reverse().join('');
};

// Examples:
// console.log(padLeft(23,5));       //=> '00023'
// console.log((23).padLeft(5));     //=> '00023'
// console.log((23).padLeft(5,' ')); //=> '   23'
// console.log(padLeft(23,5,'>>'));  //=> '>>>>>>23'

Number.prototype.padLeft = function( n, str ) {
    return Array(n - String(this).length + 1).join(str || '0') + this;
}

// For ex: "23".padLeft( "00000" )   //=> '00023'
//   Also: "23".padLeft( 5, "1" )    //=> '11123'
String.prototype.padLeft = function( n, str ) {

	// RE: http://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng
	if( arguments.length === 1 )
	    return String( n + this ).slice( -n.length );

	if( arguments.length !== 2 )
		return this;

    return Array(n - this.length + 1).join(str || '0') + this;

};

strings.test = String.prototype.test = function( s ) {
    return "test - " + s;
};

strings.upperCaseFirstLetter = function( s ) {
    return s.charAt( 0 ).toUpperCase() + s.slice( 1 )
}

String.prototype.upperCaseFirstLetter = function() {
    return this.charAt( 0 ).toUpperCase() + this.slice( 1 )
};

