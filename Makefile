REPORTER = spec

test:
	@NODE_ENV=test ./node_modules/.bin/mocha tests \
		--reporter $(REPORTER) \

test-b:
	@NODE_ENV=test ./node_modules/.bin/mocha --bail tests \
		--reporter $(REPORTER) \

test-w:
	@NODE_ENV=test ./node_modules/.bin/mocha tests \
		--reporter $(REPORTER) \
		--growl \
		--watch

.PHONY: test test-b test-w
